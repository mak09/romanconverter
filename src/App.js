import React from "react";
import logo from "./logo.svg";
import RomanConverter from "./components/RomanConverter";
import "./App.css";

function App() {
  return <RomanConverter />;
}

export default App;
