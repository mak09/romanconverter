import { toRoman } from "../services/toRoman";
describe("toRoman", () => {
  it.each`
    arabic  | expectedRoman
    ${0}    | ${"none"}
    ${1}    | ${"I"}
    ${2}    | ${"II"}
    ${3}    | ${"III"}
    ${4}    | ${"IV"}
    ${5}    | ${"V"}
    ${6}    | ${"VI"}
    ${7}    | ${"VII"}
    ${8}    | ${"VIII"}
    ${9}    | ${"IX"}
    ${10}   | ${"X"}
    ${11}   | ${"XI"}
    ${14}   | ${"XIV"}
    ${19}   | ${"XIX"}
    ${39}   | ${"XXXIX"}
    ${40}   | ${"XL"}
    ${1500} | ${"MD"}
    ${2019} | ${"MMXIX"}
  `("should return $expectedRoman for $arabic", ({ arabic, expectedRoman }) => {
    expect(toRoman(arabic)).toEqual(expectedRoman);
  });
});
