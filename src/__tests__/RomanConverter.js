import { render, cleanup, fireEvent } from "@testing-library/react";
import React from "react";
import RomanConverter from "../components/RomanConverter";

describe("<ToRoman/>", () => {
  it("should render", () => {
    expect(() => {
      render(<RomanConverter />);
    }).not.toThrow();
  });

  it("should have input field", () => {
    const romanConverter = render(<RomanConverter />);
    const { getByLabelText } = romanConverter;
    expect(() => {
      getByLabelText(/Wartość arabska/);
    }).not.toThrow();
  });

  xit("should convert 1 to I", () => {
    const romanConverter = render(<RomanConverter />);
    const { getByLabelText, getByText } = romanConverter;
    const input = getByLabelText(/Wartość arabska/);
    fireEvent.change(input, { target: { value: 1 } });
    expect(() => {
      getByText("Wartość rzymska: I");
    }).not.toThrow();
  });

  xit("should convert 5 to V", () => {
    const romanConverter = render(<RomanConverter />);
    const { getByLabelText, getByText } = romanConverter;
    const input = getByLabelText(/Wartość arabska/);
    fireEvent.change(input, { target: { value: 5 } });
    expect(() => {
      getByText("Wartość rzymska: V");
    }).not.toThrow();
  });

  xit("should convert 2019 to MMXIX", () => {
    const romanConverter = render(<RomanConverter />);
    const { getByLabelText, getByText } = romanConverter;
    const input = getByLabelText(/Wartość arabska/);
    fireEvent.change(input, { target: { value: 2019 } });
    expect(() => {
      getByText("Wartość rzymska: MMXIX");
    }).not.toThrow();
  });

  afterEach(() => {
    cleanup();
  });
});
