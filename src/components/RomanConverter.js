import React from "react";
import { toRoman } from "../services/toRoman";

class RomanConverter extends React.Component {
  state = {
    arabicValue: "",
    romanValue: "none"
  };
  handleArabicChange = event => {
    const currentValue = event.target.value;
    const romanValue = toRoman(currentValue);
    this.setState({
      arabicValue: event.target.value,
      romanValue: romanValue
    });
  };

  render() {
    const { arabicValue, romanValue } = this.state;
    return (
      <div>
        <label htmlFor="arabicValue">
          Wartość arabska
          <input
            id="arabicValue"
            type="number"
            onChange={this.handleArabicChange}
            value={arabicValue}
          />
        </label>
        <h3>Wartość rzymska: {romanValue}</h3>
      </div>
    );
  }
}

export default RomanConverter;
