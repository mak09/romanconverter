const mapping = {
  1: "I",
  4: "IV",
  5: "V",
  9: "IX",
  10: "X",
  40: "XL",
  50: "L",
  90: "XC",
  100: "C",
  400: "CD",
  500: "D",
  900: "CM",
  1000: "M"
};

export function toRoman(arabic) {
  let roman = "";
  let currentArabic = arabic;

  const valuesToFormat = [1000, 500, 100, 50, 10, 5, 1];
  valuesToFormat.forEach(value => {
    let formatted = formatRoman(currentArabic, value);
    roman += formatted.currentRoman;
    currentArabic = formatted.arabicToFormat;
  });

  return roman == "" ? "none" : roman;
}

function formatRoman(arabic, valueToFormat) {
  if (arabic == 0) {
    return {
      currentRoman: "",
      arabicToFormat: 0
    };
  }
  const roman = mapping[arabic];
  const explicitMappingExist = roman != undefined;

  if (explicitMappingExist) {
    return {
      currentRoman: roman,
      arabicToFormat: 0
    };
  }

  const result = arabic / valueToFormat;

  if (result < 1) {
    return {
      currentRoman: "",
      arabicToFormat: arabic
    };
  }
  return {
    currentRoman: mapping[valueToFormat].repeat(result),
    arabicToFormat: arabic % valueToFormat
  };
}
